# My_awesome_Calculator

Desktoprechner um Berechnungen durchzuführen und 2D- oder 3D-Graphen zu zeichnen.

## Die Berechnungen (sowohl einfache als auch die Punkte der Graphen) sollen mithilfe von Excel berechnet werden, daher muss jede Eingabe mit der Excel-Syntax übereinstimmen!!!

## Allgemeines
Es soll ein Hauptfenster geben, von welchem aus folgende Fenster geöffnet werden können:

![image](pics/mainwin.png)

## Berechnungen
Texteingabe der Berechnung (z.B.: 4*5-3) mit Ausgabe aller seit dem Öffnen des Fensters getätigten Berechnungen.

![image](pics/calc.png)

Auf Knopfdruck kann das Konverterfenster geöffnet werden:

#### Umrechner Bin/Dec/Hex/Oct
Auswahl von welcher zu welcher Basis konvertiert werden soll, plus Eingabefeld der Zahl im passenden Format zum Umrechnen in die andere Basis.

![image](pics/konverter.png)

## 2D-Graphen
Eingabe einer Funktion in ein Textfeld (y=f(x)), welche auf einem Canvas gezeichnet wird.

![image](pics/2d.png)

## 3D-Graphen
Eingabe einer Funktion in ein Textfeld (z=f(x,y)), welche in einen HelixToolkit.Wpf 3DViewport gezeichnet wird.

![image](pics/3d.png)

## Aufbau der App

![image](pics/codemap.png)

**[zur Dokumentation...](https://gitlab.com/moritzschwaerzler/my_awsome_calculator/-/wikis/home)**
