﻿using System.Windows;

namespace Calc
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btCalc_Click(object sender, RoutedEventArgs e)
        {
            WindowCalc dlgcalc = new WindowCalc();
            dlgcalc.Show();
        }

        private void bt3D_Click(object sender, RoutedEventArgs e)
        {
            Window3D dlg3d = new Window3D();
            dlg3d.Show();
        }

        private void bt2D_Click(object sender, RoutedEventArgs e)
        {
            Window2D dlg2d = new Window2D();
            dlg2d.Show();
        }
    }
}
