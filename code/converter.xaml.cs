﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Calc
{
    /// <summary>
    /// Interaktionslogik für converter.xaml
    /// </summary>
    public partial class converter : Window
    {
        Types intype = Types.Dec;
        Types outtype = Types.Bin;

        public converter()
        {
            InitializeComponent();
        }

        private void btstart_Click(object sender, RoutedEventArgs e)
        {
            string instring;
            try
            {
                instring = tbin.Text;
                if (string.IsNullOrWhiteSpace(instring)) throw new Exception("Eingabefeld darf nicht leer sein");

                tbloout.Text = Raus(Rein(instring, intype), outtype);
            }
            catch (Exception exp)
            {
                MessageBox.Show("Fehler bei der Eingabe: " + exp.Message);
            }
        }

        private string Raus(int num, Types type)
        {
            string usse = "";
            switch (type)
            {
                case Types.Dec:
                    {
                        usse = Convert.ToString(num, 10);
                        break;
                    }

                case Types.Bin:
                    {
                        usse = Convert.ToString(num, 2);
                        break;
                    }

                case Types.Hex:
                    {
                        usse = Convert.ToString(num, 16);
                        break;
                    }

                case Types.Oct:
                    {
                        usse = Convert.ToString(num, 8);
                        break;
                    }
            }
            return usse;
        }

        private int Rein(string str, Types type)
        {
            int usse = 0;
            switch (type)
            {
                case Types.Dec:
                    {
                        usse = Convert.ToInt32(str);
                        break;
                    }

                case Types.Bin:
                    {
                        usse = Convert.ToInt32(str, 2);
                        break;
                    }

                case Types.Hex:
                    {
                        usse = Convert.ToInt32(str, 16);
                        break;
                    }

                case Types.Oct:
                    {
                        usse = Convert.ToInt32(str, 8);
                        break;
                    }
            }
            return usse;
        }

        private void cobovon_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (cobovon.SelectedIndex)
            {
                case 0:
                    {
                        intype = Types.Dec;
                        break;
                    }
                case 1:
                    {
                        intype = Types.Bin;
                        break;
                    }
                case 2:
                    {
                        intype = Types.Hex;
                        break;
                    }
                case 3:
                    {
                        intype = Types.Oct;
                        break;
                    }
            }
        }

        private void cobozu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (cobozu.SelectedIndex)
            {
                case 0:
                    {
                        outtype = Types.Dec;
                        break;
                    }
                case 1:
                    {
                        outtype = Types.Bin;
                        break;
                    }
                case 2:
                    {
                        outtype = Types.Hex;
                        break;
                    }
                case 3:
                    {
                        outtype = Types.Oct;
                        break;
                    }
            }
        }
    }

    enum Types
    {
        Dec,
        Bin,
        Hex,
        Oct,
    }
}
