﻿using MVVM;
using System.Windows;
using System.Windows.Media;

namespace Calc
{
    /// <summary>
    /// Interaktionslogik für Window2D.xaml
    /// </summary>
    public partial class Window2D : Window
    {
        MVVM2D mvvm = new MVVM2D();
        bool isLoaded = false;

        public Window2D()
        {
            InitializeComponent();
            isLoaded = true;
            DataContext = mvvm;
            scrollViewer.ScrollToHorizontalOffset(500);
            scrollViewer.ScrollToVerticalOffset(250);
        }

        private void btstart_Click(object sender, RoutedEventArgs e)
        {
            mvvm.Calc();
        }

        private void slzoom_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!isLoaded) return;

            var tg = new TransformGroup();
            var st = new ScaleTransform();
            var tt = new TranslateTransform();

            tt.X = 1000;
            tt.Y = 500;

            st.ScaleX = slzoom.Value;
            st.ScaleY = slzoom.Value;

            tg.Children.Add(st);
            tg.Children.Add(tt);

            canvas.RenderTransform = tg;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            mvvm.Dispose();
        }
    }
}
