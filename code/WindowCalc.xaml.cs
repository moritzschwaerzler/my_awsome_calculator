﻿using MVVM;
using System;
using System.Windows;

namespace Calc
{
    /// <summary>
    /// Interaktionslogik für WindowCalc.xaml
    /// </summary>
    public partial class WindowCalc : Window
    {
        MVVMCalc calc = new MVVMCalc();

        public WindowCalc()
        {
            InitializeComponent();
            DataContext = calc;
        }

        private void btCalc_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                calc.Calc();
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }

        private void tbkonverter_Click(object sender, RoutedEventArgs e)
        {
            converter dlgconvert = new converter();
            dlgconvert.Show();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            calc.Dispose();
        }
    }
}
