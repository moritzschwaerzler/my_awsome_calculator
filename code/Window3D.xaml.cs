﻿using MVVM;
using System.Windows;

namespace Calc
{
    /// <summary>
    /// Interaktionslogik für Window3D.xaml
    /// </summary>
    public partial class Window3D : Window
    {
        MVVM3D mvvm = new MVVM3D();
        public Window3D()
        {
            InitializeComponent();
            DataContext = mvvm;
        }

        private void btstart_Click(object sender, RoutedEventArgs e)
        {
            mvvm.Calc();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            mvvm.Dispose();
        }
    }
}
